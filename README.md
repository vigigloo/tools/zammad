# Zammad

## Requirements

- Elasticsearch with `ingest-attachment` plugin (vendored, [helm charts](https://github.com/elastic/helm-charts/blob/main/elasticsearch/README.md) with [custom image](https://gitlab.com/vigigloo/tools/elasticsearch/container_registry/2867807?search[]=ingest-attachment) or [terraform module](https://gitlab.com/vigigloo/tools/elasticsearch/-/infrastructure_registry/5747400) with [custom image](https://gitlab.com/vigigloo/tools/elasticsearch/container_registry/2867807?search[]=ingest-attachment))
- Redis (vendored, [helm charts](https://github.com/bitnami/charts/tree/master/bitnami/redis/) or [terraform module](https://gitlab.com/vigigloo/tools/redis/-/infrastructure_registry/5746810))
- Memcached (vendored, [helm charts](https://github.com/bitnami/charts/tree/master/bitnami/memcached/) or [terraform module](https://gitlab.com/vigigloo/tools/memcached/-/infrastructure_registry/5768858))
- PostgreSQL (vendored, [helm charts](https://gitlab.com/vigigloo/tools/crunchydata-pgo-cluster/-/packages/5596708) or [terraform module](https://gitlab.com/vigigloo/tools/crunchydata-pgo-cluster/-/infrastructure_registry/5606316))

## Example deployment

<details>
<summary>This terraform snippet should allow running Zammad on minikube. It can take a while to be ready.</summary>
<br>

```hcl
module "zammad_namespace" {
  source       = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version      = "0.1.0"
  max_cpu      = 8
  max_memory   = "16Gi"
  namespace    = "tools-zammad"
  project_name = "Zammad"
  project_slug = "zammad"
}

module "elasticsearch" {
  source           = "gitlab.com/vigigloo/tools-k8s/elasticsearch"
  version          = "0.2.0"
  chart_name       = "elastic"
  namespace        = module.zammad_namespace.namespace
  image_repository = "registry.gitlab.com/vigigloo/tools/elasticsearch"
  image_tag        = "7.17.1-ingest-attachment"
  values = [
    <<-EOT
  antiAffinity: soft
  EOT
  ]

  elasticsearch_replicas         = 2
  elasticsearch_persistence_size = "1Gi"
  requests_cpu                   = "100m"
  requests_memory                = "128Mi"
  limits_cpu                     = "500m"
  limits_memory                  = "512Mi"
}

resource "random_password" "redis_password" {
  length  = 32
  special = false
}

module "redis" {
  source         = "gitlab.com/vigigloo/tools-k8s/redis"
  version        = "0.1.1"
  chart_name     = "redis"
  namespace      = module.zammad_namespace.namespace
  redis_password = resource.random_password.redis_password.result
  requests_cpu   = "100m"
  limits_cpu     = "100m"
  redis_replicas = 0
}

module "memcached" {
  source     = "gitlab.com/vigigloo/tools-k8s/memcached"
  version    = "0.1.1"
  chart_name = "memcached"
  namespace  = module.zammad_namespace.namespace
}

resource "random_password" "postgresql_password" {
  length  = 32
  special = false
}

resource "helm_release" "postgresql" {
  name      = "postgresql"
  chart     = "bitnami/postgresql"
  namespace = module.zammad_namespace.namespace
  values = [
    <<-EOT
  replicaCount: 1
  auth:
    username: zammad
    database: zammad
    password: ${resource.random_password.postgresql_password.result}
  primary:
    resources:
      requests:
        cpu: 250m
        memory: 256Mi
      limits:
        cpu: 250m
        memory: 256Mi
  EOT
  ]
}

module "zammad" {
  source     = "gitlab.com/vigigloo/tools-k8s/zammad"
  version    = "0.1.1"
  chart_name = "zammad"
  namespace  = module.zammad_namespace.namespace

  zammad_persistence_size = "2Gi"
  redis_host              = module.redis.hostname
  redis_password          = resource.random_password.redis_password.result
  memcached_host          = module.memcached.hostname
  elasticsearch_host      = module.elasticsearch.hostname
  postgresql_host         = "postgresql"
  postgresql_user         = "zammad"
  postgresql_password     = resource.random_password.postgresql_password.result
  postgresql_database     = "zammad"
}
```

</details>
