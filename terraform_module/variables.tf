variable "namespace" {
  type = string
}
variable "chart_name" {
  type = string
}
variable "chart_version" {
  type    = string
  default = "6.2.0"
}
variable "values" {
  type    = list(string)
  default = []
}
variable "image_repository" {
  type    = string
  default = "zammad/zammad-docker-compose"
}
variable "image_tag" {
  type    = string
  default = "5.1.0-4"
}

variable "helm_force_update" {
  type    = bool
  default = false
}
variable "helm_recreate_pods" {
  type    = bool
  default = false
}
variable "helm_cleanup_on_fail" {
  type    = bool
  default = false
}
variable "helm_max_history" {
  type    = number
  default = 0
}
variable "helm_timeout" {
  type    = number
  default = 600
}

variable "limits_cpu" {
  type    = string
  default = "1000m"
}
variable "limits_memory" {
  type    = string
  default = "2Gi"
}
variable "requests_cpu" {
  type    = string
  default = "1000m"
}
variable "requests_memory" {
  type    = string
  default = "2Gi"
}
