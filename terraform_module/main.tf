resource "helm_release" "zammad" {
  chart           = "zammad"
  repository      = "https://zammad.github.io/zammad-helm"
  name            = var.chart_name
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history
  timeout         = var.helm_timeout

  values = concat([
    file("${path.module}/zammad.yaml"),
  ], var.values)

  dynamic "set" {
    for_each = var.zammad_persistence_size == null ? [] : [var.zammad_persistence_size]
    content {
      name  = "persistence.size"
      value = var.zammad_persistence_size
    }
  }

  dynamic "set" {
    for_each = var.postgresql_host == null ? [] : [var.postgresql_host]
    content {
      name  = "zammadConfig.postgresql.host"
      value = var.postgresql_host
    }
  }
  dynamic "set" {
    for_each = var.postgresql_user == null ? [] : [var.postgresql_user]
    content {
      name  = "zammadConfig.postgresql.user"
      value = var.postgresql_user
    }
  }
  dynamic "set" {
    for_each = var.postgresql_password == null ? [] : [var.postgresql_password]
    content {
      name  = "zammadConfig.postgresql.pass"
      value = var.postgresql_password
    }
  }
  dynamic "set" {
    for_each = var.postgresql_database == null ? [] : [var.postgresql_database]
    content {
      name  = "zammadConfig.postgresql.db"
      value = var.postgresql_database
    }
  }

  dynamic "set" {
    for_each = var.elasticsearch_host == null ? [] : [var.elasticsearch_host]
    content {
      name  = "zammadConfig.elasticsearch.host"
      value = var.elasticsearch_host
    }
  }
  set {
    name  = "zammadConfig.elasticsearch.initialisation"
    value = var.elasticsearch_initialisation
  }

  dynamic "set" {
    for_each = var.redis_host == null ? [] : [var.redis_host]
    content {
      name  = "zammadConfig.redis.host"
      value = var.redis_host
    }
  }
  dynamic "set" {
    for_each = var.redis_password == null ? [] : [var.redis_password]
    content {
      name  = "zammadConfig.redis.pass"
      value = var.redis_password
    }
  }

  dynamic "set" {
    for_each = var.memcached_host == null ? [] : [var.memcached_host]
    content {
      name  = "zammadConfig.memcached.host"
      value = var.memcached_host
    }
  }

  dynamic "set" {
    for_each = var.image_repository == null ? [] : [var.image_repository]
    content {
      name  = "image.repository"
      value = var.image_repository
    }
  }
  dynamic "set" {
    for_each = var.image_tag == null ? [] : [var.image_tag]
    content {
      name  = "image.tag"
      value = var.image_tag
    }
  }

  dynamic "set" {
    for_each = var.limits_cpu == null ? [] : [var.limits_cpu]
    content {
      name  = "zammadConfig.railsserver.resources.limits.cpu"
      value = var.limits_cpu
    }
  }
  dynamic "set" {
    for_each = var.limits_memory == null ? [] : [var.limits_memory]
    content {
      name  = "zammadConfig.railsserver.resources.limits.memory"
      value = var.limits_memory
    }
  }
  dynamic "set" {
    for_each = var.requests_cpu == null ? [] : [var.requests_cpu]
    content {
      name  = "zammadConfig.railsserver.resources.requests.cpu"
      value = var.requests_cpu
    }
  }
  dynamic "set" {
    for_each = var.requests_memory == null ? [] : [var.requests_memory]
    content {
      name  = "zammadConfig.railsserver.resources.requests.memory"
      value = var.requests_memory
    }
  }
}
