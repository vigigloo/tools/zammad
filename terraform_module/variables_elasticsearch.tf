variable "elasticsearch_host" {
  type = string
}
variable "elasticsearch_initialisation" {
  type    = bool
  default = false
}
