variable "postgresql_host" {
  type = string
}
variable "postgresql_user" {
  type    = string
  default = "zammad"
}
variable "postgresql_password" {
  type = string
}
variable "postgresql_database" {
  type    = string
  default = "zammad"
}
